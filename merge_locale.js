/*
 * Utility for helping identifying missing translation keys.
 *
 * Usage: node merge_locale.js > output
 *
 * Now using a diff tool between output and app/locale/pt-BR/app.json it is
 * easy to spot missing keys.
 */
var merge = require('merge');

var en = require('./app/locale/en/app.json');
var ptBR = require('./app/locale/pt-BR/app.json');

var result = merge.recursive(true, en, ptBR);
console.log(JSON.stringify(result, null, 2));
