var fs = require('fs');
var assign = require('object-assign');
var webdriver = require('selenium-webdriver');
var test = require('selenium-webdriver/testing');
var chai = require('chai');
var chaiWD = require('chai-webdriver');
var chaiPromise = require('chai-as-promised');
var helper = require('./deglet_helper');

var screenshots = './output/';
var alphabet = {
  withWithSpace: [32, 95],  // from ' ' (space) to '~'
  visible: [33, 94]         // from '!' (space) to '~'
};


/* Functions to run before and after tests. */
function setupTest() {
  var driver = new webdriver.Builder().forBrowser('firefox').build();
  chai.use(chaiWD(driver));
  chai.use(chaiPromise);
  return driver;
}

test.before(function() {
  this.driver = setupTest();
});

test.after(function() {
  this.driver.quit();
});


function saveScreenshot(data, filename) {
  fs.writeFileSync(screenshots + filename, data, 'base64');
}


module.exports = assign({}, helper, {

  randomString: function(length, range) {
    length = length || 8;
    range = range || alphabet.visible;
    var str = '';
    for (var i = 0; i < length; i++) {
      str += String.fromCharCode(range[0] + Math.floor((Math.random() * range[1])));
    }
    return str;
  },

  saveScreenshot: saveScreenshot,

  capture: function(driver, filename) {
    driver.takeScreenshot().then(function(data) {
      saveScreenshot(data, filename);
    });
  },

  fill: function(driver, data) {
    Object.keys(data).forEach(function(key) {
      var elem = driver.findElement({name: key});
      elem.clear();
      elem.sendKeys(data[key]);
    });
  },

  submit: function(driver, selector) {
    var selector = selector || {css: 'button[type=submit]'};
    var elem = driver.findElement(selector);
    elem.click();
  },

  setupTest: setupTest

});
