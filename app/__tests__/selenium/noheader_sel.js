var test = require('selenium-webdriver/testing');
var expect = require('chai').expect;
var base = require('./base');


test.describe('Deglet - display without header', function() {

  test.it('shoud load index without header', function(done) {
    this.driver.get(base.baseUrl + '?header=0');
    expect(this.driver.getTitle()).to.eventually.equal(base.siteTitle);
    expect('.logo-bg').dom.not.to.be.visible();
    base.capture(this.driver, 'noheader.png');
    done();
  });

});
