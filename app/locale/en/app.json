{
  "header": {
    "title": "Deglet"
  },

  "welcome": {
    "title": "Deglet",
    "subtitle": "An open source, multi-signature Bitcoin wallet.",
    "user": "Hello {name}",
    "feature_bitcoin_1": "Buy, Sell, Send and Receive bitcoins.",
    "feature_bitcoin_2": "Simple, guaranteed pricing.",
    "feature_bitcoin_3": "Any volume, worldwide."
  },

  "login": {
    "title": "My account",
    "subtitle": "Create a new wallet for free or access your wallet.",
    "username": "Username",
    "password": "Password",
    "password_new": "Password (at least {num} characters)",
    "pwdminlen": "Enter at least {num} characters",
    "weakpwd": "Please use a password with at least {num} characters",
    "submitting": "Grabbing data..",
    "generatingkey": "Deriving keys..",
    "button": "Log in",
    "newuser": "New user? {linkSignup}",
    "existinguser": "Got an account? {linkLogin}"
  },

  "signup": {
    "tosagree": "By signing up, you agree to our Terms of service.",
    "passwordrepeat": "Repeat your password",
    "button": "Open it"
  },

  "send": {
    "title": "Transfer some amount",
    "amount": "Amount to send",
    "success": "Transaction created, now confirm to send it.",
    "button": "Send bitcoins"
  },

  "receive": {
    "simple": {
      "label": "Receiving address",
      "button_title": "Get a new address",
      "loading": "Loading address..",
      "info_show": "Show address information",
      "info_hide": "Hide address information",
      "info_created_on": "Created on",
      "info_derivation_path": "Derivation path"
    },
    "invoice": {
      "label": "Amount to receive",
      "button": "Generate invoice",
      "created": "Created invoice {id}, share its link with the payer.",
      "option": {
	"show": "Show options",
	"hide": "Hide options",
	"description": "Invoice title",
	"description_len": "At max {num} characters"
      }
    }
  },

  "convert": {
    "title": "Convert balance",
    "from": "From",
    "to": "To",
    "switch": "Switch",
    "switch_title": "Swap currencies",
    "button": "Start conversion",
    "button_title": "You'll have 7 minutes to confirm it.",
    "success": "Conversion {id} created! Open it and finish the process."
  },

  "history": {
    "recordtitle": "{num, plural,  =0 {No transactions were found}  =1 {1 transaction}  other {# transactions}}",
    "empty": "No history",
    "button": {
      "filtertoggle": "Filter",
      "clearfilter": "Clear filters",
      "filter": "Apply",
      "export": "Export to CSV",
      "export_small": "Export"
    },
    "filter_by": {
      "id_or_address": "Transaction ID (partial is ok) or Address",
      "id_or_destaddress": "Transaction ID (partial is ok) or Recipient address",
      "type": "Transaction type",
      "from_date": "Between this date",
      "to_date": "And this date"
    },
    "table": {
      "when": "When",
      "type": "Type",
      "status": "Status"
    }
  },

  "transaction": {
    "unknown": "Unknown transaction",
    "type": {
      "sent": "Sent",
      "received": "Received",
      "moved": "Sent to myself"
    },
    "status": {
      "waiting_user": "Waiting user",
      "pending": "Pending",
      "paid": "Paid",
      "confirmed": "Paid and confirmed",
      "expired": "Expired",
      "canceled": "Canceled"
    }
  },

  "settings": {
    "account_type": "Account type",
    "account_type_info": "{network} - {m} of {n}",
    "public_keyring": "Public keyring"
  },

  "page_notfound": {
    "message": "Well, this page is gone. Or maybe it never existed.",
    "wallet_link": "Go back to my wallet."
  },

  "widget": {
    "balance": {
      "title": "Balance",
      "available": "Available",
      "loading": "Please wait..",
      "see_history": "See history"
    },
    "pendingtx": {
      "title": "Pending transactions",
      "empty": "No transactions waiting for you {iconOk}"
    },
    "ticker": {
      "title": "Ticker",
      "last": "Last",
      "day": "24h",
      "month": "Month"
    },
    "current_address": {
      "title": "Current receiving address",
      "hint": "For more, visit the {receiveLink} page."
    },
    "txdetail": {
      "title": "Transaction {id}",
      "blocktime": "First confirmation",
      "unconfirmed": "Not confirmed yet",
      "transtime": "Received time",
      "totalinput": "Total input",
      "totaloutput": "Total output",
      "outputs": "Received bitcoins",
      "spent": "spent"
    },
    "newuser": {
      "title": "Welcome to your new wallet",
      "tip": "Your password is not recoverable.",
      "tip_long": "Your password never leaves your computer and it is used for locally encrypting your wallet. The ability to decrypt your wallet is lost in case you forget your password.",
      "accept": "Ok, I'll be careful"
    },
    "address": {
      "title": "Look up address",
      "lookup": "Is it mine?",
      "is_mine": "{address} belongs to your account.",
      "is_partially_mine": "{address} belongs to your account but is managed externally.",
      "is_not_mine": "{address} does not belong to your account."
    }
  },

  "invoice": {
    "date": "Date",
    "received_from": "Received from",
    "sent_through": "Sent through",
    "nowhere": "Nowhere",
    "payment_amount": "Amount to send",
    "payment_address": "Payment address",
    "recipient_address": "Recipient",
    "qr_showaddress": "Show only address",
    "qr_showuri": "Show bitcoin URI",
    "countdown_start": "Start payment",
    "countdown_hint": "You'll have {duration} minutes to send the payment.",
    "retry": "Retry",
    "retry_hint": "Please retry in a few moments and enter in contact if the issue persists.",
    "gooduntil": "Invoice valid until {date}",
    "timeleft": "Time left",
    "expired_at": "Expired at {date}",
    "status": "Status",
    "pay_with_bitcoin": "Payment method: Bitcoin",
    "rate": "Rate",
    "paynow": "Pay transaction",
    "paying": {
	"starting": "Starting payment..",
	"signing": "Signing transaction..",
	"paid_wait": "Payment is complete, waiting for provider.."
    },
    "conversion": "Conversion"
  },

  "generic": {
    "destination": "Destination",
    "amount": "Amount",
    "fees": "Fees",
    "action": "Action",
    "loading": "Loading..",
    "reject": "Reject",
    "confirm": "Confirm",
    "hide": "Hide",
    "show": "Show",
    "confs": "Confirmations",
    "input": "Input",
    "output": "Output",
    "address": "Address",
    "hint_close": "Close this hint",
    "total": "Total",
    "due": "Due",
    "paid": "Paid",
    "origin": "Origin",
    "help": "Help",
    "error": "Error",
    "bad_error": "Error!",
    "close": "Close",
    "tip": "Tip",
    "learn_more": "Learn more",
    "source_code": "Source code"
  },

  "release": {
    "description": "Beta release - sharp & pointy"
  },

  "lang": {
    "en_name": "English",
    "ptBR_name": "Portuguese (Brazil)"
  },

  "link": {
    "wallet": "Wallet",
    "home": "Home",
    "send": "Send",

    "receive": "Receive",
    "recvsimple": "Simple receive",
    "recvinvoice": "Create invoice",

    "convert": "Convert",

    "history": "History",
    "histbitcoin": "Bitcoin transactions",
    "histexternal": "Invoices & Conversions",

    "settings": "Settings",
    "settings_account": "Account",
    "settings_transactions": "Transactions",

    "help": "Help",

    "logout": "Log out",
    "login": "Log In",
    "login_long": "Log in",
    "signup": "Sign Up",
    "signup_long": "Create an account"
  },

  "error": {
    "server_comm": "Failed to communicate with the server.",
    "no_wallet": "Wallet still loading.",
    "wallet_not_ready": "Wallet still being created.",
    "invalid_address": "Address specified is invalid.",
    "amount_too_low": "Amount specified is too low.",
    "balance": {
      "insufficient": "Amount specified is above your balance.",
      "sufficient_butlocked": "Amount specified is above your available balance.",
      "sufficient_butfees": "Amount specified is above your balance after the network fee."
    },
    "blockchain": {
      "utxo": "Server failed to fetch your unspent outputs.",
      "history": "Server failed to fetch your transaction history.",
      "tx": "Server failed to fetch transaction."
    },
    "backoff_time": "Please wait some moments before sending a request.",
    "unknown": "Uh oh! An unknown error!",
    "login": {
      "creds": "Credentials rejected.",
      "passwordmismatch": "Passwords do not match."
    },
    "invoice": {
      "sign_fail": "Signing failed!",
      "external_fail": "Server failed to send the payment."
    },
    "deglet": {
      "bad_id": "Invalid ID",
      "unknown_id": "Unknown transaction ID",
      "unknown_pair": "The currency pair specified is not known",
      "address_in_use": "Address already used in an invoice or conversion",
      "state_update": "Server failed to update the transaction status",
      "unknown_side": "Side specificied for conversion is not known",
      "unknown_currency": "Currency specified is not known",
      "no_conversion": "No conversion specified",
      "already_started": "Countdown already started",
      "bad_payment": "BTC -> USD payment must originate from your side",
      "bad_state": "Account got into an inconsistent state!",
      "unknown_user": "Username not found, please check it",
      "user_already_exists": "Username is not available, try a different one",
      "wallet_complete": "Tried to update a wallet that is already complete",
      "no_blob": "Encrypted wallet was not returned",
      "cosign": "Cosigner failed to sign transaction",
      "join_cosigner": "Cosigner failed to join your wallet",
      "trans_fetch": "Failed to fetch transaction"
    },
    "outofsync": {
      "title": "Server is still synchronizing.",
      "description": "While this process is going on, your wallet will not be fully operational. Please try loading it again later."
    }
  },

  "calendar": {
    "previousMonth": "Previous Month",
    "nextMonth": "Next Month",
    "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    "weekdays": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    "weekdaysShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
  }
}
