"use strict";

var BWC = require('bitcore-wallet-api');
var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var sjcl = require('../util/sjcl');
var storage = require('./storage');
var BWS_URL = require('../constant/ServerConstants').BWS_URL;
var DEGLET_URL = require('../constant/ServerConstants').DEGLET_SOCKETIO;
var debug = require('../debug')(__filename);

var _wallet;


function _initialWallet() {
  return {
    raw: null,         /* Wallet data */
    listening: false,  /* Listening for socket-io events? (one time setup) */
    connected: false,  /* Connected to the BWS server? */
    ready: false,      /* Is the wallet ready to be used (is it complete)? */
    needCreds: false,  /* Does the user need to re-enter credentials? */
    client: null,      /* Client to send requests to BWS */
    pending: null,     /* Transactions waiting to be cosigned. */
    addresses: null, /* XXX */
    info: {
      balance: null,
      address: null,   /* Last address created. */
      error: null
    }
  };
}


function _updateBalance(data) {
  _wallet.info.balance = {
    locked: data.lockedAmount,
    total: data.totalAmount,
    currency: 'BTC'
  };
  storage.walletInfo.save(_wallet.client.credentials.walletId, _wallet.info);
}


function _updateWallet(data) {
  var raw = JSON.parse(data);
  storage.wallet.save(raw);
  _wallet.raw = raw;
  if (_wallet.client) {
    _wallet.ready = _wallet.client.credentials.isComplete();
  }
}


_wallet = _initialWallet();


var WalletStore = BaseStore.create();


WalletStore.getData = function() {
  return _wallet;
};


WalletStore.setupClient = function(loginData) {
  var result = {local: true};

  if (_wallet.client !== null) {
    /* Do not create a client if one is already present. */
    return result;
  }

  var client = new BWC({
    verbose: process.env.NODE_ENV === "debug" ? true : false,
    baseUrl: BWS_URL,
    degletUrl: DEGLET_URL
  });
  client.removeAllListeners();

  if (loginData.blob) {
    /* There is a local wallet, load from it. */
    if (loginData.key) {
      var raw = sjcl.decrypt(loginData.key, loginData.blob);
      _updateWallet(raw);
      client.import(raw);
      _wallet.client = client;
      _wallet.needCreds = false;
    } else {
      /* Encryption key is missing, ask for credentials to generate it. */
      _wallet.needCreds = true;
    }
    this.emitChange();
    return result;
  }

  /* No wallet in the server, create one here now. */
  result.local = false;
  result.client = client;
  return result;

  /* NOTE: WalletAction is the place that will initiate socket-io
   * notifications. That is done in order to dispatch events related
   * to this from WalletAction.
   */
};


WalletStore.cleanupClient = function() {
  if (_wallet.client !== null) {
    _wallet.client.removeAllListeners();
    if (_wallet.listening) {
      _wallet.client.stopNotifications();
    }
  }
  _wallet = _initialWallet();
  this.emitChange();
};


WalletStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.WALLET_CREATED: {
      debug('new wallet created');
      _wallet.client = action.data;
      _updateWallet(_wallet.client.export());
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_LISTENING: {
      debug('client is ready to receive wallet events');
      _wallet.listening = true;
      _wallet.connected = true;
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_CONNECTED: {
      debug('client connected');
      _wallet.connected = true;
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_DISCONNECTED: {
      debug('client disconnected');
      _wallet.connected = false;
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_BALANCE: {
      if (!_wallet.client) {
        return;
      }
      _updateBalance(action.data);
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_STATUS: {
      if (!_wallet.client) {
        return;
      }
      _updateBalance(action.data.balance);
      if (!_wallet.ready && action.data.wallet.status === 'complete') {
        _updateWallet(_wallet.client.export());
      }
      _wallet.pending = action.data.pendingTxps;
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_OPEN: {
      if (!_wallet.client) {
        return;
      }
      var creds = _wallet.client.credentials;
      debug('wallet-open', action.data, creds.isComplete());
      _updateWallet(_wallet.client.export());
      _wallet.info = storage.walletInfo.get(creds.walletId);
      _wallet.info.error = null;
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_NEW_ADDRESS: {
      _wallet.info.address = action.data;
      storage.walletInfo.save(action.data.walletId, _wallet.info);
      debug('wallet-new-address', _wallet.info.address);
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_ADDRESSES: {
      _wallet.addresses = action.data.map(function(item) {
        return {address: item.address, change: item.isChange};
      });
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.WALLET_ERROR: {
      _wallet.info.error = action.error;
      WalletStore.emitChange();
      break;
    }

    case ActionTypes.LOGOUT: {
      /* Remove any wallet data from local storage. */
      if (_wallet.client && _wallet.client.credentials) {
        storage.walletInfo.erase(_wallet.client.credentials.walletId);
      }
      storage.wallet.erase();
      WalletStore.emitChange();
      break;
    }

  }

});


module.exports = WalletStore;
