"use strict";

var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var History = require('./History');
var AppDispatcher = require('../dispatcher');
var EventConstants = require('../constant/EventConstants');
var ActionTypes = require('../constant/ActionTypes');
var ErrorKey = require('../util/errorkey');
var debug = require('../debug')(__filename);

var _sortableColumns = ['time', 'amount', 'fees'];
var _filterableColumns = ['txid', 'action', 'time', 'addressTo'];
var _history = new History.History(_sortableColumns, _filterableColumns);

_history.historyData = _history.initialHistoryData();


var WalletHistoryStore = assign({}, EventEmitter.prototype, {

  getData: function() {
    return _history.historyData;
  },

  sort: function(column) {
    var didSort = _history.localSort(column);
    if (didSort) {
      this.emitChange();
    }
    return didSort;
  },

  filter: function(match) {
    var didFilter = _history.localFilter(match);
    if (didFilter) {
      this.emitChange();
    }
    return didFilter;
  },

  clearFilters: function() {
    var allClean = _history.clearFilters();
    if (allClean) {
      this.emitChange();
    }
    return allClean;
  },

  setCurrPage: function(pageNum) {
    var changed = _history.setCurrPage(pageNum);
    if (changed) {
      this.emitChange();
    }
    return changed;
  },

  emitChange: function() {
    this.emit(EventConstants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(EventConstants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(EventConstants.CHANGE_EVENT, callback);
  },

  emitDownload: function(path) {
    this.emit(EventConstants.DOWNLOAD_EVENT, path);
  },

  addDownloadListener: function(callback) {
    this.on(EventConstants.DOWNLOAD_EVENT, callback);
  },

  removeDownloadListener: function(callback) {
    this.removeListener(EventConstants.DOWNLOAD_EVENT, callback);
  }

});


WalletHistoryStore.dispatchToken = AppDispatcher.register(function(action) {

  switch (action.actionType) {

    case ActionTypes.WALLET_HISTORY_FETCH: {
      var client = action.data.client;
      var firstPage = action.data.firstPage;
      var download = action.data.download;
      var opts = History.prepareQuery(_history, firstPage, download);

      client.getTxHistory(opts, function(err, data) {
        debug('getTxHistory', err);
        _history.historyData.error = err ? ErrorKey.bws(err) : '';
        if (!download) {
          if (!err) {
            data.firstPage = firstPage;
            _history.updateRawData(data);
          }
          WalletHistoryStore.emitChange();
        } else {
          WalletHistoryStore.emitDownload(data ? data.filename : null);
        }
      });

      break;
    }

    case ActionTypes.LOGOUT: {
      /* Cleanup. */
      _history.historyData = _history.initialHistoryData();
      break;
    }

  }

});

module.exports = WalletHistoryStore;
