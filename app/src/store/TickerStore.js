"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var request = require('../util/request');

var _timer = null;
var _defaultPair = 'BTCUSD';
var _ticker = {
  'BTCUSD': null
};

var TickerStore = BaseStore.create();


TickerStore.getData = function(pair) {
  return _ticker[pair];
};


TickerStore.start = function() {
  return;
};

TickerStore.stop = function() {
  window.clearTimeout(_timer);
  _timer = null;
};


TickerStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.TICKER: {
      _ticker[action.ticker.pair] = action.ticker;
      TickerStore.emitChange();
      break;
    }

  }

});


function getTicker() {
  var pair = _defaultPair;
  request.get(
    '/ticker/' + pair, null,
    function(ticker) {
      _ticker[pair] = ticker;
      TickerStore.emitChange();
    },
    function(err, info) {
      console.log('err get ticker', err, info);
      _timer = setTimeout(getTicker, 5000);
    }
  );
}

getTicker();


module.exports = TickerStore;
