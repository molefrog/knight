"use strict";

var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var EventConstants = require('../constant/EventConstants');
var AppDispatcher = require('../dispatcher');
var ErrorKey = require('../util/errorkey');


function create() {
  var store = assign({}, EventEmitter.prototype, {

    setDispatcher: function(registerFunc) {
      this.dispatchToken = AppDispatcher.register(registerFunc);
    },

    emitChange: function() {
      this.emit(EventConstants.CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
      this.on(EventConstants.CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
      this.removeListener(EventConstants.CHANGE_EVENT, callback);
    }

  });

  return store;
}


function parseDegletError(info) {
  var key;
  var msg;

  try {
    msg = JSON.parse(info.text).message;
    key = ErrorKey.deglet({error: msg});
  } catch (e) {
    key = null;
  }

   return key;
}


module.exports = {
  create: create,
  parseDegletError: parseDegletError
};
