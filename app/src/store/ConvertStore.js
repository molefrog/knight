"use strict";

var BaseStore = require('./BaseStore');
var ActionTypes = require('../constant/ActionTypes');
var errorkey = require('../util/errorkey');
var request = require('../util/request');
var debug = require('../debug')(__filename);

var _transaction;


function _emptyTransaction() {
  return {
    obj: null,
    error: null
  };
}

_transaction = _emptyTransaction();


var ConversionStore = BaseStore.create();


function transactionSuccess(data) {
  if (data.error) {
    /* Status 200 but contains a validation error. */
    _transaction.error = errorkey.deglet(data);
  } else {
    _transaction.obj = data;
    _transaction.error = null;
  }
  ConversionStore.emitChange();
}


function transactionError(err, info) {
  debug("transaction failed: " + err, info);

  var key = BaseStore.parseDegletError(info);
  if (key === null) {
    key = 'error.deglet.trans_fetch';
  }
  _transaction.error = key;

  ConversionStore.emitChange();
}


ConversionStore.getData = function() {
  return _transaction;
};


ConversionStore.clear = function() {
  _transaction = _emptyTransaction();
};


ConversionStore.setDispatcher(function(action) {

  switch (action.actionType) {

    case ActionTypes.CONVERT_CREATE: {
      request.post('/conversion', action.data,
                   transactionSuccess, transactionError);
      break;
    }

  }

});


module.exports = ConversionStore;
