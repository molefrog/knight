"use strict";


var History = function(sortableColumns, filterableColumns) {
  this.sortableColumns = sortableColumns;
  this.filterableColumns = filterableColumns;
  this.historyData = null;
};


History.prototype.initialSort = function(sortTime) {
  var cfg = {};

  this.sortableColumns.map(function(entry) {
    cfg[entry] = 0;
    if (sortTime && entry === 'time') {
      /* By default entries are sorted in descending order by timestamp. */
      cfg[entry] = -1;
    }
  });

  return cfg;
};


History.prototype.initialFilter = function() {
  var cfg = {};

  this.filterableColumns.map(function(column) {
    cfg[column] = null;
  });

  return cfg;
};


History.prototype.initialHistoryData = function() {
  return {
    rawHistory: null,
    rawTotal: 0,
    error: '',

    history: null,
    resultsPerPage: 10,
    pageCount: 1,
    currPage: 1,
    total: 0,

    sort: this.initialSort(true),
    filter: this.initialFilter(),
    filtered: false,
    allAvailable: false,

    pageLoad: 10,       /* Number of pages to load from the server. */
    pagesAvail: [0, 0]
  };
};


History.prototype.numPages = function(n) {
  return Math.ceil(n / this.historyData.resultsPerPage) || 1;
};


History.prototype.updateData = function(historyData) {
  var total = historyData.total;
  var pageCount = this.numPages(total);
  var firstPage = historyData.firstPage || 1;

  /* When modifying the following make sure to /not/ do it in-place.
   * this.historyData.rawHistory points to the same data and it shouldn't
   * be modified.
   */
  this.historyData.history = historyData.data;

  this.historyData.filtered = historyData.filtered;
  this.historyData.total = total;
  this.historyData.pageCount = pageCount;
  this.historyData.currPage = firstPage;

  /* Calculate which pages are available. */
  this.historyData.pagesAvail = [
    firstPage,
    firstPage + this.numPages(historyData.data.length) - 1
  ];
};


History.prototype.updateRawData = function(historyData) {
  /* Update based on data received from the server. */

  this.historyData.rawHistory = historyData.data;
  this.historyData.rawTotal = historyData.total;

  if (historyData.filtered || historyData.total !== historyData.data.length) {
    /* If filtering was performed server-side, or just a subset of pages
     * available were fetched, then sorting (and possibly filtering) will
     * always be done in the server.
     */
    this.historyData.allAvailable = false;
  } else {
    /* Sorting and filtering can be done locally without missing results. */
    this.historyData.allAvailable = true;
  }

  /* Make this new data the active data. */
  this.updateData(historyData);
};


History.prototype.updateSortConfig = function(column) {
  var cfg = this.historyData.sort[column];

  /* Clear any active sort config. */
  this.historyData.sort = this.initialSort(false);

  /* Flip the sort order for this column so the sort happens in the
   * inverse order in relation to the last call. */
  if (cfg === 0) {                             /* Natural order. */
    this.historyData.sort[column] = -1;        /* Descending. */
  } else {
    this.historyData.sort[column] = cfg * -1;  /* Flip between asc and desc. */
  }
};


History.prototype.activeSort = function() {
  /* Find and return the active sort column. */
  for (var i = 0; i < this.sortableColumns.length; i++) {
    var column = this.sortableColumns[i];
    if (this.historyData.sort[column]) {
      return {
        column: column,
        order: this.historyData.sort[column]
      };
    }
  }
};


History.prototype.sortRows = function(cfg) {
  var column = cfg.column;
  var order = cfg.order;
  var data = this.historyData.history.slice(0);  // Shallow copy.

  /* Sort based on the active sort configuration. */
  this.historyData.history = data.sort(function(a, b) {
    if (a[column] < b[column]) {
      return -1 * order;
    } else if (a[column] > b[column]) {
      return 1 * order;
    } else {
      return 0;
    }
  });
};


History.prototype.filterRows = function() {
  var data = this.historyData.rawHistory;
  if (!data) {
    return;
  }

  var n = 0;
  var fmatch = {};
  this.filterableColumns.map(function(column) {
    if (this.historyData.filter[column]) {
      fmatch[column] = this.historyData.filter[column];
      n++;
    }
  }, this);

  if (!n) {
    /* Why are we here if there is no active filter? */
    throw new Error("no filters active");
  }

  var filtered = [];
  for (var i = 0; i < data.length; i++) {
    var row = data[i];
    var matches = 0;

    if (fmatch.txid && row.txid.match(fmatch.txid)) {
      matches++;
    }
    if (fmatch.id && row.id.match(fmatch.id)) {
      matches++;
    }
    if (fmatch.payment_address && row.payment_address === fmatch.payment_address) {
      matches++;
    }
    if (fmatch.addressTo && row.addressTo === fmatch.addressTo) {
      matches++;
    }
    if (fmatch.action && fmatch.action.indexOf(row.action) !== -1) {
      matches++;
    }

    if (fmatch.time) {
      if (!fmatch.time.to && fmatch.time.from && row.time >= fmatch.time.from) {
        /* Unbounded max date. */
        matches++;
      } else if (fmatch.time.to && !fmatch.time.from && row.time <= fmatch.time.to) {
        /* Unbounded min date. */
        matches++;
      } else if (row.time >= fmatch.time.from && row.time <= fmatch.time.to) {
        matches++;
      }
    }

    if (matches === n) {
      /* Row passed through all active filters. */
      filtered.push(row);
    }
  }

  /* Make the filtered data the active one. */
  this.updateData({
    filtered: true,
    data: filtered,
    total: filtered.length
  });
};


History.prototype.clearFilters = function() {
  this.updateData({
    filtered: false,
    data: this.historyData.rawHistory,
    total: this.historyData.rawTotal
  });
  this.historyData.filter = this.initialFilter();

  if (!this.historyData.allAvailable) {
    /* Need to fetch original unfiltered data from the server. */
    return false;
  }

  /* Need to sort again if there is any sort in place. */
  var sortCfg = this.activeSort();
  if (sortCfg) {
    this.sortRows(sortCfg);
  }

  return true;
};


History.prototype.localSort = function(column) {

  if (typeof this.historyData.sort[column] !== 'number') {
    /* This column is not supposed to be sortable. */
    return null;
  }

  if (this.historyData && this.historyData.history.length > 1 &&
      typeof this.historyData.history[0][column] !== 'undefined') {

    this.updateSortConfig(column);

    var numPages = this.numPages(this.historyData.total);
    if (!this.historyData.allAvailable) {
      if (this.historyData.pagesAvail[0] !== 1 ||
          this.historyData.pagesAvail[1] !== numPages) {
        /* Need to sort in the server.
          * One or more pages are not available here.
          **/
        return false;
      }
    }

    this.sortRows(this.activeSort());
    return true;
  }
};


History.prototype.localFilter = function(match) {
  this.filterableColumns.map(function(column) {
    /* Set active filters but also erase others. */
    var query = match[column];
    this.historyData.filter[column] = query ? query : null;
  }, this);

  if (!this.historyData.allAvailable) {
    /* Filter server-side. */
    return false;
  }

  this.filterRows();
  return true;
};


History.prototype.setCurrPage = function(pageNum) {
  if (pageNum <= 0 || pageNum === this.historyData.currPage) {
    return null;
  }

  if (pageNum < this.historyData.pagesAvail[0] ||
      pageNum > this.historyData.pagesAvail[1]) {
    /* Range of pages available do not include the page that
     * the user wants to see. */
    return false;
  }

  this.historyData.currPage = pageNum;
  return true;
};


function prepareQuery(history, firstPage, download) {
  var skip = (firstPage - 1) * history.historyData.resultsPerPage;
  var filterData = [];
  var opts;

  history.filterableColumns.map(function(column) {
    var query = history.historyData.filter[column];
    if (query) {
      query = (typeof query === 'object') ? query : query.toString();
      filterData.push({name: column, query: query});
    }
  });

  opts = {
    filter: filterData || null,
    limit: history.historyData.resultsPerPage * history.historyData.pageLoad,
    sort: history.activeSort(),
    skip: skip,
    download: download ? download.toString() : null
  };

  return opts;
}


module.exports = {
  History: History,
  prepareQuery: prepareQuery
};
