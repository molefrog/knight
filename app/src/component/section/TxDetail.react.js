"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var WalletAction = require('../../action/WalletAction');
var TxStore = require('../../store/TxDetailStore');

var template = require('../../../template/section/TxDetail.jsx');


var TxDetail = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    return {
      txid: null,
      data: null,
      loading: true
    };
  },

  componentDidMount: function() {
    TxStore.addChangeListener(this._onChange);

    var txid = this.props.params.txid;
    if (txid) {
      this.setState({txid: txid}, function() {
        this._loadTx();
      });
    }
  },

  componentWillReceiveProps: function(props) {
    var txid = props.params.txid;

    if (txid !== this.state.txid) {
      this.setState({txid: txid}, function() {
        this._loadTx();
      });
    }
  },

  componentWillUnmount: function() {
    TxStore.removeChangeListener(this._onChange);
  },

  _loadTx: function() {
    var txid = this.state.txid;
    this.setState({loading: true}, function() {
      WalletAction.txInfo(txid);
    });
  },

  _onChange: function() {
    this.setState({
      data: TxStore.getData(),
      loading: false
    });
  },

  render: template

});


module.exports = TxDetail;
