"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var template = require('../../../template/section/Help.jsx');


var Help = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  render: template

});


module.exports = Help;
