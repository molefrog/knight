"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var ActionTypes = require('../../constant/ActionTypes');
var WalletStore = require('../../store/WalletStore');
var WalletAction = require('../../action/WalletAction');
var qrcode = require('../../util/qrcode');
var template = require('../../../template/section/ReceiveSimple.jsx');


var ReceiveSimple = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    var addressInfo = WalletStore.getData().info.address;
    var qrImg = null;
    if (addressInfo) {
      qrImg = qrcode.buildBitcoinQR(addressInfo.address).img;
    }

    return {
      showAddressInfo: false,
      addressInfo: addressInfo,

      submitted: false,
      err: '',
      qrSrc: qrImg,
      qrSize: qrcode.imgSize
    };
  },

  componentDidMount: function() {
    WalletStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    WalletStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
    var winfo = WalletStore.getData().info;
    if (winfo.error && winfo.error.name === ActionTypes.WALLET_NEW_ADDRESS) {
      /* Some error occurred while creating a new address. */
      /* XXX Error is not being displayed to the user yet. */
      this.setState({submitted: false, err: winfo.error.reason.error});
      return;
    }

    var addressInfo = winfo.address;
    if (!addressInfo) {
      /* Some other change from the WalletStore that does not matter here. */
      return;
    }

    var addressChanged = addressInfo !== this.state.addressInfo;

    if (addressChanged || this.state.qrSrc === null) {
      var updatedState = {qrSrc: qrcode.buildBitcoinQR(addressInfo.address).img};

      if (addressChanged) {
        updatedState.submitted = false;
        updatedState.addressInfo = addressInfo;
      }

      this.setState(updatedState);
    }
  },

  _onSubmit: function(event) {
    event.preventDefault();
    this.setState({submitted: true, err: ''}, function() {
      var client = this.props.wallet.client;
      if (!client) {
        console.log('client not available');
        return;
      }
      WalletAction.newAddress(client);
    });
  },

  _onGetAddress: function(event) {
    var target = event.target;
    setTimeout(function() {
      target.select();
    }, 0);
  },

  _onToggleInfo: function(event) {
    event.preventDefault();

    var show = this.state.showAddressInfo;
    this.setState({showAddressInfo: !show});
  },

  render: template

});


module.exports = ReceiveSimple;
