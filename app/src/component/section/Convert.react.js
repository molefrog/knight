"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var ConvertStore = require('../../store/ConvertStore');
var ConvertAction = require('../../action/ConvertAction');
var WalletAction = require('../../action/WalletAction');
var LoginStore = require('../../store/LoginStore');
var TickerStore = require('../../store/TickerStore');
var template = require('../../../template/section/Convert.jsx');


var currencySelect = [
  {value: 'BTC', label: 'BTC'},
  {value: 'USD', label: 'USD'}
];


var Convert = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    return {
      ticker: TickerStore.getData('BTCUSD'),
      submitting: false,
      err: null,
      info: null,

      fromAmount: 1,
      fromCurrency: 'BTC',
      toAmount: null,
      toCurrency: 'USD',
      specified: 'from'
    };
  },

  componentDidMount: function() {
    TickerStore.start();
    TickerStore.addChangeListener(this._onTickerChange);
    ConvertStore.addChangeListener(this._onConvertTrans);

    if (this.state.ticker) {
      this._calcAmount();
    }

    var width = window.innerWidth || 1000;
    if (width >= 768) {
      var node = React.findDOMNode(this.refs.fromAmount);
      node.focus();
      node.select();
    }
  },

  componentWillUnmount: function() {
    TickerStore.stop();
    TickerStore.removeChangeListener(this._onTickerChange);
    ConvertStore.removeChangeListener(this._onConvertTrans);
  },

  selectOptions: currencySelect,

  _onTickerChange: function() {
    /* XXX Always BTCUSD. */
    var ticker = TickerStore.getData('BTCUSD');
    this.setState({ticker: ticker}, this._calcAmount);
  },

  _calcAmount: function() {
    /* Re-calculate one amount. */
    var ticker = this.state.ticker.last;
    var side = this.state.specified;
    var amountSide = this.state[side + 'Amount'];
    var currencySide = this.state[side + 'Currency'];
    var amount;

    if (currencySide === 'BTC') {
      /* BTC -> USD */
      amount = (amountSide * ticker).toFixed(2);
    } else {
      /* USD -> BTC */
      amount = (amountSide / ticker).toFixed(8);
    }

    if (side === 'from') {
      this.setState({toAmount: amount});
    } else {
      this.setState({fromAmount: amount});
    }
  },

  _onSubmit: function(event) {
    event.preventDefault();

    var uid = LoginStore.getData().uid;
    var client = this.props.wallet.client;
    /* XXX Same code is present in Send.react and ReceiveInvoice.react */
    if (!client || !uid) {
      this.setState({err: 'error.no_wallet'});
      return;
    } else if (!this.props.wallet.ready) {
      this.setState({err: 'error.wallet_not_ready'});
      return;
    }

    var address = this.props.wallet.info.address.address;
    var side = this.state.specified;
    var amount;
    var currency;
    var otherCurrency;

    if (side === 'from') {
      amount = this.state.fromAmount;
      currency = this.state.fromCurrency;
      otherCurrency = this.state.toCurrency;
    } else {
      amount = this.state.toAmount;
      currency = this.state.toCurrency;
      otherCurrency = this.state.fromCurrency;
    }

    this.setState({submitting: true, err: null, info: null}, function() {
      ConvertAction.create(uid, address, side, amount, currency, otherCurrency);
    });
  },

  _onToAmount: function(event) {
    var newAmount = event.target.value;
    this.setState(
      {toAmount: newAmount, specified: 'to'},
      this._calcAmount
    );
  },

  _onFromAmount: function(event) {
    var newAmount = event.target.value;
    this.setState(
      {fromAmount: newAmount, specified: 'from'},
      this._calcAmount
    );
  },

  _onSwitch: function(event) {
    if (event) {
      event.preventDefault();
    }

    this.setState({
      fromCurrency: this.state.toCurrency,
      toCurrency: this.state.fromCurrency
    }, this._calcAmount);
  },

  _onFromCurrency: function(event) {
    var newValue = event.target.value;
    this._updateCurrency('fromCurrency', newValue, this.state.toCurrency);
  },

  _onToCurrency: function(event) {
    var newValue = event.target.value;
    this._updateCurrency('toCurrency', newValue, this.state.fromCurrency);
  },

  _updateCurrency: function(key, value, opposite) {
    if (value === opposite) {
      /* New currency selection is the same as the opposite one,
       * switch them. */
      this._onSwitch();
    } else {
      var update = {};
      update[key] = value;
      this.setState(update, this._calcAmount);
    }
  },

  _onConvertTrans: function() {
    /* Conversion created. */
    var store = ConvertStore.getData();
    var conversion = store.obj;
    if (store.error) {
      this.setState({submitting: false, err: store.error});
      return;
    }

    /* Update receiving address. */
    var client = this.props.wallet.client;
    WalletAction.newAddress(client);

    /* Now the user can open the conversion and pay for it. */
    this.setState({submitting: false, info: conversion.id});

    /* Open modal with the conversion. */
    var self = this;
    setTimeout(function() {
      self.showModal();
    }, 250);
  },


  showModal: function() {
    var height = window.innerHeight || 700;
    if (height < 640) {
      /* This window is too short to display this modal properly.
       * Instead, the user should open the transaction in its own page.
       */
      return;
    }

    if (this.state.info) {
      this.refs.modal.show();
    }
  },

  hideModal: function() {
    this.refs.modal.hide();
  },

  onHideMessage: function() {
    this.setState({err: null, info: null});
  },

  render: template

});


module.exports = Convert;
