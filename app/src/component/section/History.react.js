"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var template = require('../../../template/section/History.jsx');


var History = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  render: template

});


module.exports = History;
