"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');

var template = require('../../../template/section/Header.jsx');


var HeaderSection = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.State],

  propTypes: {
    username: React.PropTypes.string.isRequired
  },

  render: template

});


module.exports = HeaderSection;
