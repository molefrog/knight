"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var WalletAction = require('../../action/WalletAction');
var WalletSendAction = require('../../action/WalletSendAction');
var WalletStore = require('../../store/WalletStore');
var WalletSendStore = require('../../store/WalletSendStore');
var WalletCosignStore = require('../../store/WalletCosignStore');
var PayExternalStore = require('../../store/PayExternalStore');

var InvoiceAction = require('../../action/InvoiceAction');
var InvoiceStore = require('../../store/InvoiceStore');
var AppConstant = require('../../constant/App');
var qrcode = require('../../util/qrcode');
var template = require('../../../template/widget/Invoice.jsx');


function _initTimer() {
  return {
    duration: AppConstant.invoice.duration,
    start: null,
    count: 0,
    left: AppConstant.invoice.duration,
    expired: false,
    expireAt: null
  };
}


function timerCount(timer) {
  /* Calculate how many seconds have passed since the timer started.
   * If the timer has expired, the invoice's duration is returned.
   */
  var count;
  var timeNow = new Date().getTime() / 1000;
  if (timeNow > timer.expireAt) {
    count = timer.duration;
  } else {
    count = parseInt(timeNow - timer.start);
  }

  return count;
}


var InvoiceWidget = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  propTypes: {
    hideBottom: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      invoiceId: null,

      loadingRate: false,
      timer: _initTimer(),
      qrUri: null,          /* QR code for the bitcoin:address?amount=xxx uri. */
      qrAddress: null,      /* QR code for the address only. */
      qrShowUri: true,      /* Show the QR code for the uri by default. */

      payStatus: '',       /* Inline payment status for conversions. */

      error: null,
      data: null
    };
  },

  componentDidUpdate: function() {
    if (this.props.params.id === this.state.invoiceId) {
      return;
    }

    var initialState = this.getInitialState();
    initialState.invoiceId = this.props.params.id;

    this.clearCountdown();
    this.setState(initialState, this.fetchInvoice);
  },

  componentDidMount: function() {
    this.setState({invoiceId: this.props.params.id}, this.fetchInvoice);
    window.addEventListener('resize', this.onResize);
    InvoiceStore.addChangeListener(this._onInvoiceChange);
    InvoiceStore.addRefreshListener(this._onInvoiceRefresh);

    /* Other stores required for completing conversion payments. */
    WalletSendStore.addChangeListener(this._onBitcoinPayment);
    WalletCosignStore.addChangeListener(this._onPaymentCosigned);
    PayExternalStore.addChangeListener(this._onExternalPayment);
  },

  componentWillUnmount: function() {
    this.clearCountdown();
    window.removeEventListener('resize', this.onResize);
    InvoiceStore.clear();
    InvoiceStore.removeChangeListener(this._onInvoiceChange);
    InvoiceStore.removeRefreshListener(this._onInvoiceRefresh);

    WalletSendStore.removeChangeListener(this._onBitcoinPayment);
    WalletCosignStore.removeChangeListener(this._onPaymentCosigned);
    PayExternalStore.removeChangeListener(this._onExternalPayment);
  },

  selectElement: function(event) {
    event.target.select();
  },

  _onInvoiceChange: function() {
    var store = InvoiceStore.getData();
    var data = store.obj;
    if (store.error) {
      this.setState({error: store.error});
      return;
    }

    var update = {loadingRate: false, data: data};
    if (data.status_info[0].status === 'pending') {
      /* Show countdown. */
      update.timer = this.state.timer;
      update.timer.start = data.status_info[0].date.epoch_time;
      update.timer.expireAt = data.status_info[0].expireAt.epoch_time;
      update.timer.count = timerCount(update.timer);
      update.timer.left = update.timer.duration - update.timer.count;

      /* Build QR code. */
      update.qrUri = qrcode.buildBitcoinQR(
        data.payment_address,
        (data.input.amount / 1e8).toFixed(8)
      );
      update.qrAddress = qrcode.buildBitcoinQR(data.payment_address, null, '');

      this.setState(update, this.initCountdown);
    } else {
      /* Countdown is not running yet. */
      this.setState(update);
    }
  },

  _onInvoiceRefresh: function() {
    var data = InvoiceStore.getData().obj;
    var lastUpdate = this.state.data.last_update.epoch_time;
    var update = {};

    if (data.last_update.epoch_time !== lastUpdate) {
      /* Invoice was actually modified, refresh it. */
      if (data.status_info[0].status !== 'pending') {
        /* Countdown is no longer in effect. */
        this.clearCountdown();
      } else if (data.input.received) {
        /* Possibly update QR code. */
        update.qrUri = qrcode.buildBitcoinQR(
          data.payment_address,
          ((data.input.amount - data.input.received.total) / 1e8).toFixed(8)
        );
      }
      update.data = data;
      this.setState(update);
    }
  },

  onResize: function() {
    /* Recalculate width. */
    this.updateProgressBar();
  },

  fetchInvoice: function() {
    InvoiceAction.load(this.state.invoiceId);
  },

  onCalcRate: function(event) {
    event.preventDefault();

    var iid = this.state.invoiceId;
    if (!iid) {
      /* Invoice not loaded yet. */
      return;
    }

    this.setState({loadingRate: true}, function() {
      InvoiceAction.loadRate(iid);
    });
  },

  onRetry: function() {
    window.location.reload();
  },

  barTimer: null,

  refreshTimer: null,

  initCountdown: function() {
    var self = this;

    this.updateProgressBar();

    this.barTimer = setInterval(function() {
      var update = {timer: self.state.timer};
      update.timer.count = timerCount(self.state.timer);
      self.setState(update, self.updateProgressBar);
    }, 1000);

    this.refreshTimer = setInterval(function() {
      InvoiceAction.refresh(self.state.invoiceId);
    }, 1250);
  },

  clearCountdown: function() {
    window.clearInterval(this.barTimer);
    window.clearInterval(this.refreshTimer);
    this.barTimer = null;
    this.refreshTimer = null;
  },

  updateProgressBar: function() {
    var width = React.findDOMNode(this.refs.defaultBar).offsetWidth;
    var currentWidth = width;
    var progressBar = React.findDOMNode(this.refs.progressBar);
    var updateState = true;
    var duration = this.state.timer.duration;
    var timeLeft = duration - this.state.timer.count;
    var update = {timer: this.state.timer};

    update.timer.left = timeLeft;

    if (this.state.timer.expired) {
      /* Timer already expired.
       * This is used only to resize the bar when the window is resized.
       */
      updateState = false;
    }

    if (!timeLeft) {
      if (this.barTimer) {
        window.clearInterval(this.barTimer);
        this.barTimer = null;
      }
      update.timer.expired = true;
    } else {
      currentWidth = (timeLeft * width) / duration;
    }

    if (updateState) {
      this.setState(update);
    }

    progressBar.style.width = currentWidth + 'px';
  },

  toggleQr: function(event) {
    event.preventDefault();

    var showUri = this.state.qrShowUri;
    this.setState({qrShowUri: !showUri});
  },

  onPayTransaction: function(event) {
    event.preventDefault();

    var tid = this.state.data.id;
    var satoshi = this.state.data.input.amount;
    var currency = this.state.data.input.currency;
    var address = this.state.data.payment_address;

    if (typeof tid !== 'string') {
      this.setState({payStatus: 'error.no_transaction'});
      return;
    }

    if (currency === 'USD') {
      /* Pay from deglet db, no bitcoin client needed. */
      this.setState({payStatus: 'invoice.paying.starting'}, function() {
        WalletAction.payConversion(tid);
      });
    } else {
      /* Pay from client-side bitcoin wallet. */
      var client = WalletStore.getData().client;
      if (!client) {
        this.setState({payStatus: 'error.no_wallet'});
        return;
      }
      this.setState({payStatus: 'invoice.paying.starting'}, function() {
        WalletSendAction.submit(client, address, satoshi);
      });
    }
  },

  _onBitcoinPayment: function() {
    var info = WalletSendStore.getData();
    var txid = info.id;

    if (info.error) {
      this.setState({payStatus: info.msg});
      return;
    }

    /* Cosign transaction proposal now. */
    var client = WalletStore.getData().client;
    if (!client) {
      this.setState({payStatus: 'error.no_wallet'});
      return;
    }
    this.setState({payStatus: 'invoice.paying.signing'}, function() {
      WalletAction.cosignRequest(client, txid);
    });
  },

  _onPaymentCosigned: function() {
    var info = WalletCosignStore.getData();
    if (info.error) {
      this.setState({payStatus: 'error.invoice.sign_fail'});
    } else {
      this.setState({payStatus: 'invoice.paying.paid_wait'});
      /* Transaction is expected to move to the paid status quickly. */
    }
  },

  _onExternalPayment: function() {
    var info = PayExternalStore.getData();
    console.log(info);
    if (info.error) {
      this.setState({payStatus: 'error.invoice.external_fail'});
    } else {
      this.setState({payStatus: 'invoice.paying.paid_wait'});
      /* Transaction is expected to move to the paid status quickly. */
    }
  },

  render: template

});


module.exports = InvoiceWidget;
