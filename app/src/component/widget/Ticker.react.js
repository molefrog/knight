"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var TickerStore = require('../../store/TickerStore');
var template = require('../../../template/widget/Ticker.jsx');


var Ticker = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  propTypes: {
    pair: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
  },

  getInitialState: function() {
    return {
      data: TickerStore.getData(this.props.pair[0] + this.props.pair[1])
    };
  },

  componentDidMount: function() {
    TickerStore.start();
    TickerStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    TickerStore.stop();
    TickerStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
    var data = TickerStore.getData(this.props.pair[0] + this.props.pair[1]);
    this.setState({data: data});
  },

  render: template

});


module.exports = Ticker;
