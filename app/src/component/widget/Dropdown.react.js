"use strict";

var React = require('react');
var ClickOutside = require('react-onclickoutside');
var template = require('../../../template/widget/Dropdown.jsx');


var DropdownWidget = React.createClass({

  mixins: [ClickOutside],

  propTypes: {
    title: React.PropTypes.string,
    options: React.PropTypes.arrayOf(React.PropTypes.shape(
      {
        value: React.PropTypes.string.isRequired,
        label: React.PropTypes.string.isRequired
      }
    )).isRequired
  },

  getInitialState: function() {
    return {
      active: false,
      selected: {}
    };
  },

  _toggle: function(event) {
    var target = event.target;

    if (target !== React.findDOMNode(this.refs.dropdown) &&
        target !== React.findDOMNode(this.refs.right) &&
        target !== React.findDOMNode(this.refs.title)) {

      /* Clicked on some item. */
      if (typeof target.checked !== 'undefined' && target.value) {
        var selected = this.state.selected;
        selected[target.value] = target.checked;
        this.setState({selected: selected});
      }

      return;
    }

    this.setState({active: !this.state.active});
  },

  handleClickOutside: function() {
    this.setState({active: false});
  },

  getSelected: function() {
    var selection = [];
    Object.keys(this.state.selected).forEach(function(key) {
      if (this.state.selected[key]) {
        selection.push(key);
      }
    }, this);
    return selection;
  },

  render: template

});


module.exports = DropdownWidget;
