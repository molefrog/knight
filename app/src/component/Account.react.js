"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');
var LoginStore = require('../store/LoginStore');
var WalletStore = require('../store/WalletStore');
var WalletNotificationStore = require('../store/WalletNotificationStore');
var WalletAction = require('../action/WalletAction');
var debug = require('../debug')(__filename);
var errorkey = require('../util/errorkey');

var template = require('../../template/Account.jsx');
var indexLink = require('../constant/AppLinks').name.index;


var Account = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.State],

  statics: {
    willTransitionTo: function(transition) {
      var user = LoginStore.getData().username;
      if (!user) {
        /* User not logged in, send back to the initial page. */
        transition.redirect(indexLink);
      }
    }
  },

  getInitialState: function() {
    return {
      login: LoginStore.getData(),
      wallet: WalletStore.getData(),
      notification: WalletNotificationStore.getData(),
      error: null
    };
  },

  componentDidMount: function() {
    debug('mounted');
    WalletNotificationStore.addChangeListener(this._onNotification);
    WalletStore.addChangeListener(this._onChange);
    var self = this;
    setTimeout(function() {
      var result = WalletStore.setupClient(self.state.login);
      if (result.client) {
        /* This is a new user, create a wallet locally now. */
        WalletAction.create(result.client, self.state.login);
      }
    }, 250);
  },

  componentWillUnmount: function() {
    debug('unmounting');
    WalletNotificationStore.removeChangeListener(this._onNotification);
    WalletStore.removeChangeListener(this._onChange);
    WalletStore.cleanupClient();
  },

  _onNotification: function() {
    this.setState({notification: WalletNotificationStore.getData()});
  },

  _onChange: function() {
    debug('updated');
    var wallet = WalletStore.getData();
    var update = {wallet: wallet, error: null};
    if (wallet.info.error &&
        errorkey.bws(wallet.info.error.reason) === 'error.blockchain.utxo') {
      update.error = 'error.outofsync';
    }

    this.setState(update, function() {
      if (this.state.error) {
        this._showError();
      }

      var wallet = this.state.wallet;
      if (wallet.needCreds) {
        // WalletNotificationStore.askCreds();
        debug('ok I need to ask credentials');
      } else if (!wallet.listening && wallet.client) {
        /* Start listening for socket-io events. */
        debug('trying to start to listen...');
        WalletAction.startListening(wallet.client);
      } else if (wallet.ready && !wallet.info.address && wallet.client) {
        /* Get an address now. XXX */
        debug('getting an address...');
        WalletAction.newAddress(wallet.client);
      }
    });
  },

  _showError: function() {
    this.refs.errModal.show();
  },

  _onHideError: function(event) {
    event.preventDefault();
    this.refs.errModal.hide();
  },

  render: template

});


module.exports = Account;
