"use strict";

var namespace = require('./constant/App').log.ns;
var debug = require('debug');

module.exports = function(name) {
  return debug(namespace + ':' + name);
};
