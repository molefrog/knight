/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Router = require('react-router');
var classNames = require('classnames');

var bundle = require('../src/constant/App').bundle;
var HeaderSection = require('../src/component/section/Header.react');
var FooterSection = require('../src/component/section/Footer.react');
var SlideMenu = require('../src/component/widget/SlideMenu.react');


function template() {
  var invoiceContent = this.isActive('invoice') || this.isActive('conversion');
  var noHeader = this.getQuery().header === '0';

  return (
    <div className="body">
      {/* Show the mobile menu button only if there is an user
        * and the screen is small enough. */}
      <SlideMenu ref="mobileMenu" showButton={this.state.username && !noHeader ? true : false} />

      <HeaderSection username={this.state.username} />
      <main className={classNames('site-content', {user: this.state.username},
                                  {'no-header': noHeader,
                                   'invoice-content': invoiceContent})}>
        <div className='inner-content' ref="siteContent">
          <Router.RouteHandler/>
        </div>
      </main>
      <FooterSection username={this.state.username} />
      <script src={"/js/" + bundle}></script>
    </div>
  );
}

module.exports = template;
