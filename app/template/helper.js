"use strict";

var SYMBOLDIGITS = {
  BTC: 8,
  USD: 2
};


function formatAmount(amount, symbol) {
  var dig = SYMBOLDIGITS[symbol];
  if (!dig) {
    throw new Error(symbol + " not configured");
  }

  var fmtAmount = +(Math.pow(10, -dig) * amount).toFixed(dig);
  var fmt = fmtAmount + " " + symbol;
  return fmt;
}


module.exports = {
  formatAmount: formatAmount
};
