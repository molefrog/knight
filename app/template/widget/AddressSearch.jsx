/*jscs:disable maximumLineLength */

"use strict";

var React = require('react');
var Modal = require('boron/FadeModal');
var LaddaButton = require('react-ladda');
var BoxMessage = require('../../src/component/widget/BoxMessage.react');


function template() {
  var info = this.state.address ? 'some info' : <i>No address specified.</i>;
  var boxMsg;
  var rawMsg;

  if (this.state.info) {
    rawMsg = this.getIntlMessage(this.state.info);
    boxMsg = this.formatMessage(rawMsg, {address: this.state.address});
  }

  return (
    <div className="wallet-address-search">
      <p className="small-margin">{this.getIntlMessage('widget.address.title')}</p>
      <form onSubmit={this._onSubmit} autoCorrect="off" autoCapitalize="none">
        <input type="text" placeholder={this.getIntlMessage('generic.address')} ref="address" />
        <LaddaButton buttonStyle="zoom-in" loading={this.state.loading}
                     type="submit" className="action-button">
          {this.getIntlMessage('widget.address.lookup')}
        </LaddaButton>{/* &nbsp; <a href="#">Show info</a>*/}
      </form>

      <BoxMessage
        error={this.state.error}
        msg={boxMsg} rawMsg={rawMsg}
        onHide={this._onHideMessage} />

      <Modal ref="modal" className="simple-modal addysearch-modal">
        <div className="simple-modal-wrapper">
          <h3>{this.state.address}</h3>
          <p>{info}</p>
          <a href="#" onClick={this._onHideModal}>{this.getIntlMessage('generic.close')}</a>
        </div>
      </Modal>
    </div>
  );
}


module.exports = template;
