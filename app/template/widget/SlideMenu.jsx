/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var classNames = require('classnames');
var AppLinks = require('../../src/constant/AppLinks');
var helper = require('../helper/layout.jsx');


function template() {

  var walletHighlight = helper.walletHighlight(this);

  function listLink(item) {
    if (item.parent !== null) {
      return null;
    }

    return (
      <li key={item.link + '-mobile'}
          className={classNames({separator: item.separate === true})}>
        <Link to={item.link} onClick={this.toggleMenu}
              className={classNames({active: item.link === AppLinks.name.wallet && walletHighlight})}>
          {this.getIntlMessage(item.i18n)}
        </Link>
      </li>
    );
  }

  return (
    <div>
      <button className={classNames("menu-button", {"show-tiny": this.props.showButton})}
              onClick={this.toggleMenu}>
        <span></span>
      </button>

      <nav className="slideout-menu">
        <ul>
          {AppLinks.list.map(listLink, this)}
        </ul>
      </nav>

      <div className="slideout-mask" onClick={this.closeMenu} onTouchEnd={this.closeMenu}>
      </div>
    </div>
  );
}


module.exports = template;
