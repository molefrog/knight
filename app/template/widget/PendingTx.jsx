/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var FormattedMessage = require('react-intl').FormattedMessage;


function template() {
  var info = this.props.info;
  var loading = (info === null) ? true : false;
  var body = (info || []).map(function(item, index) {
    return (
      <tr key={item.id} className="pendingtx-row">
        <td className="hide-small">{info.length - index}</td>
        <td data-label={this.getIntlMessage('generic.destination')} className="pendingtx-addy">{item.toAddress}</td>
        <td data-label={this.getIntlMessage('generic.amount')}>{item.amount / 1e8} BTC</td>
        <td data-label={this.getIntlMessage('generic.fees')}>{item.fee / 1e8} BTC</td>
        <td data-label={this.getIntlMessage('generic.action')} className="pendingtx-action">
          <a href="#" onClick={this._rejectTx.bind(this, item.id)}>
            {this.getIntlMessage('generic.reject')}
          </a>
          <a href="#" className="action-button" onClick={this._cosignRequest.bind(this, item.id)}>
            {this.getIntlMessage('generic.confirm')}
          </a>
        </td>
      </tr>
    );
  }, this).reverse();
  var empty = (
    <tr>
      <td colSpan="5">
        <FormattedMessage
          message={this.getIntlMessage('widget.pendingtx.empty')}
          iconOk={<span className="icon-thumbsup"></span>} />
      </td>
    </tr>
  );

  return (
    <div className="pendingtx">
      <table className="user-table">
        <caption className={(loading || this.state.waiting) ? "pendingtx-loading" : null}>
          {this.getIntlMessage('widget.pendingtx.title')}
        </caption>
        <thead style={{display: loading || !body.length ? "none" : "table-row-group"}}>
          <tr>
            <th className="hide-small">#</th>
            <th>{this.getIntlMessage('generic.destination')}</th>
            <th>{this.getIntlMessage('generic.amount')}</th>
            <th>{this.getIntlMessage('generic.fees')}</th>
            <th></th>
          </tr>
        </thead>
        <tbody className={this.state.waiting ? "loading" : ""}>
          {!loading ?
            (body.length ? body : empty) :
            <tr><td colSpan="5">{this.getIntlMessage('generic.loading')}</td></tr>}
        </tbody>
      </table>
    </div>
  );
}


module.exports = template;
