"use strict";

var React = require('react');
var Link = require('react-router').Link;
var formatAmount = require('../helper').formatAmount;


function showBalance(info) {
  var available;

  if (typeof info.currency !== 'string') {
    return null;
  }

  available = info.total - (info.locked || 0);

  return (
    <tr key={"balance-" + info.currency}>
      <td>{formatAmount(info.total, info.currency)}</td>
      <td className="balance-value">{formatAmount(available, info.currency)}</td>
    </tr>
  );
}


function template() {
  var result;
  var balance = this.state.balance;
  var present = 0;

  for (var i = 0; i < balance.length; i++) {
    if (typeof balance[i] === 'object') {
      present++;
    }
  }

  if (present) {
    result = (
      <div className="balance">
        <table>
          <thead>
            <tr>
              <th className="balance-title">{this.getIntlMessage('widget.balance.title')}</th>
              <th className="balance-title">{this.getIntlMessage('widget.balance.available')}</th>
            </tr>
          </thead>
          <tbody style={{display: this.state.show ? "table-row-group" : "none"}}>
            {balance.map(showBalance)}
          </tbody>
        </table>
        {present !== balance.length ? <p>{this.getIntlMessage('widget.balance.loading')}</p> : null}
        <div className="balance-link">
          <a onClick={this.onToggle}>
            {this.state.show ?
              this.getIntlMessage('generic.hide') :
              this.getIntlMessage('generic.show')}
          </a>
          <Link to="history">{this.getIntlMessage('widget.balance.see_history')}</Link>
        </div>
      </div>
    );
  } else {
    result = (
      <div className="balance">
        <p>{this.getIntlMessage('widget.balance.loading')}</p>
      </div>
    );
  }

  return result;
}


module.exports = template;
