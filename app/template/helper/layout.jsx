"use strict";

var React = require('react');
var AppLinks = require('../../src/constant/AppLinks');


function sectionActive(obj, section) {
  /* Return true if one a page belonging to a given section is active.
   *
   * NOTE: This assumes the mixin Router.State is available.
   **/
  var linkName = obj.getPathname().slice(1);  // Drop the slash.
  var link = AppLinks.list[AppLinks.linkIndex[linkName]];
  var active = link && link[section] === true;

  return active;
}


function activeTxDetail(obj) {
  /* Return the active txid if the transaction detail info is active.
   *
   * NOTE: This assumes the mixin Router.State is available.
   **/

  var pieces = obj.getPathname().split('/');
  if (pieces.length === 3 && pieces[1] === AppLinks.name.txinfo) {
    return pieces[2];
  }
}


function walletHighlight(obj) {
  /* Return true if the wallet top link should be highlighted.
   * This is used to tweak the behavior from react-router, which
   * will already sets the "active" class on the element when it is
   * active. The intention here is to highlight it when any of the
   * wallet pages are active, even though they are not children of that
   * other page.
   *
   * NOTE: This assumes the mixin Router.State is available.
   **/

  var active = sectionActive(obj, 'wallet');
  var highlight = active && !obj.isActive(AppLinks.name.wallet);
  return highlight;
}


function row(label, colClass, innerElem) {
  /* Create a row (or two), typically in a form.
   * Resulting layout:
   *
   * <div>
   *   <div> <!-- Only if label is specified ->
   *     <div class="row">
   *       <div class="col-xs-12">
   *         <label>label</label>
   *       </div>
   *     </div>
   *   </div>
   *   <div class="row">
   *     <div class=colClass>
   *       innerElem
   *     </div>
   *   </div>
   * </div>
   *
   **/
  var labelElem = label ? row(null, null, <label>{label}</label>) : null;
  return (
    <div>
      {labelElem}
      <div className="row">
        <div className={colClass || "col-xs-12"}>
          {innerElem}
        </div>
      </div>
    </div>
  );
}


function rowAddon(label, addon, innerElem) {
  /* Similar to row(...) but integrates an add-on to the right of innerElem.
   * Typical use case is to produce the following result:
   *  ______________ ___
   * | Input        | A |
   * |______________|___|
   *
   **/
  var addonElem;
  if (typeof addon === "string") {
    addonElem = <button type="button" className="button right-addon wireframe" disabled>{addon}</button>;
  } else {
    addonElem = addon;
  }

  return (
    <div>
      {label ? row(null, null, <label>{label}</label>) : null}
      <div className="row">
        <div className="col-xs contain-right-addon">
          {innerElem}
        </div>
        {addonElem}
      </div>
    </div>
  );
}


module.exports = {
  walletHighlight: walletHighlight,
  activeTxDetail: activeTxDetail,
  sectionActive: sectionActive,

  row: row,
  rowAddon: rowAddon
};
