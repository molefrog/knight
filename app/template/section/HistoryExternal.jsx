/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var TableFilter = require('../../src/component/TableFilter.react');
var helper = require('../helper/history.jsx');
var fmtAmount = require('../helper').formatAmount;


function template() {
  var i18n = this.getIntlMessage;
  var hist = this.state.store.history || [];

  /* Make sure the results visible are only for this page. */
  var store = this.state.store;
  var firstPage = store.pagesAvail[0];
  hist = hist.slice(
    (store.currPage - firstPage) * store.resultsPerPage,
    (store.currPage - firstPage + 1) * store.resultsPerPage
  );

  function histFormat(entry) {
    var key = entry.id + "-ehist";
    var status = entry.status_info[0].status;
    var type = entry.input.received ? "invoice" : "conversion";

    return (
      <tr key={key}>
        <td className="txid">
          <Link to={type} params={{id: entry.id}}>{entry.id}</Link>
        </td>
        <td data-label={i18n('history.table.when')}>{this.formatDate(entry.time * 1000, 'shortWithTime')}</td>
        <td data-label={i18n('generic.input')}>{fmtAmount(entry.input.amount, entry.input.currency)}</td>
        <td data-label={i18n('generic.output')}>{fmtAmount(entry.output.amount, entry.output.currency)}</td>
        <td data-label={i18n('history.table.status')}>{status ? i18n('transaction.status.' + status) : null}</td>
      </tr>
    );
  }

  return (
    <div className="user-page">

      <div className="row info-row">
        {helper.status(this, hist)}
        {helper.filterButtons(this, hist.length, this._onToggleFilter, this._onClearFilters)}
      </div>
      <TableFilter
        ref="tablefilter"
        dateFilter={true}
        show={this.state.showFilter}
        filtering={this.state.loading}
        onFilter={this._onApplyFilter}/>

      <table className="user-table">
        <thead>
          <tr>
            <th>ID</th>
            <th>{i18n('history.table.when')}</th>
            <th>{i18n('generic.input')}</th>
            <th>{i18n('generic.output')}</th>
            <th>{i18n('history.table.status')}</th>
          </tr>
        </thead>

        <tbody className={this.state.loading ? "loading" : null}>
          {hist.map(histFormat, this)}
        </tbody>
      </table>

      <div className="row middle-xs table-footer">
        <div className="col-xs-8 pagination">
          {helper.paginate(
            this.state.store.currPage,
            this.state.store.pageCount,
            this._onPaginate)}
        </div>
      </div>
    </div>
  );
}


module.exports = template;
