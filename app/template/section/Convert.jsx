"use strict";

var React = require('react');
var LaddaButton = require('react-ladda');
var FormattedMessage = require('react-intl').FormattedMessage;
var Link = require('react-router').Link;
var Modal = require('boron/FadeModal');
var util = require('util');

var BoxMessage = require('../../src/component/widget/BoxMessage.react');
var Ticker = require('../../src/component/widget/Ticker.react');
var Balance = require('../../src/component/widget/Balance.react');
var Invoice = require('../../src/component/widget/Invoice.react');
var helper = require('../helper/layout.jsx');


var digits = {
  'BTC': 8,
  'USD': 2
};


function limit(val, n) {
  var re = new RegExp('\.(\\d{' + (n + 1) + ',})$');
  return val.toString().replace(re, function(x) {
    return x.slice(0, n + 1);  /* period and then n digits. */
  });
}


function template() {
  var side = this.state.specified;
  var infoMsg;
  var rawMsg;
  var summary;

  if (side === 'from') {
    summary = util.format(
      '%s %s → %s',
      limit(this.state.fromAmount, digits[this.state.fromCurrency]),
      this.state.fromCurrency,
      this.state.toCurrency
    );
  } else {
    summary = util.format(
      '%s → %s %s',
      this.state.fromCurrency,
      limit(this.state.toAmount, digits[this.state.toCurrency]),
      this.state.toCurrency
    );
  }

  if (this.state.info) {
    rawMsg = this.getIntlMessage('convert.success');
    infoMsg = (
      <FormattedMessage
        message={rawMsg}
        id={<Link to="conversion" params={{id: this.state.info}} target="_blank">{this.state.info}</Link>}
      />
    );
  } else if (this.state.err) {
    rawMsg = this.getIntlMessage(this.state.err);
    infoMsg = rawMsg;
  }

  return (
    <div className="user-page">

      <p>{this.getIntlMessage('convert.title')}</p>
      <hr/>

      <div className="row">
        <div className="col-xs-12 col-sm-8">

          <form onSubmit={this._onSubmit}>
            <div className="row bottom-xs">

              <div className={"col-xs-12 col-sm-5" + (
                              side === 'from' ? ' convert-selected' : '')}>
                {helper.rowAddon(
                  this.getIntlMessage('convert.from'),
                  <div className="select-dropdown right-addon button">
                    <select onChange={this._onFromCurrency} value={this.state.fromCurrency}>
                      {this.selectOptions.map(function(opt) {
                        return <option key={"from-" + opt.value} value={opt.value}>{opt.label}</option>;
                      })}
                    </select>
                  </div>,
                  <input type="number" placeholder={this.getIntlMessage('generic.amount')}
                         className="has-right-addon" ref="fromAmount"
                         value={this.state.fromAmount} onChange={this._onFromAmount}
                         min="0.00000001" step="any" required/>)
                }
              </div>

              <div className="col-xs-12 col-sm-2 center-sm convert-switch">
                <button onClick={this._onSwitch} className="button dark-button"
                        title={this.getIntlMessage('convert.switch_title')} type="button">
                 {this.getIntlMessage('convert.switch')} <span className="icon-exchange"></span>
                </button>
              </div>

              <div className={"col-xs-12 col-sm-5" + (
                              side === 'to' ? ' convert-selected' : '')}>
                {helper.rowAddon(
                  this.getIntlMessage('convert.to'),
                  <div className="select-dropdown right-addon button">
                    <select onChange={this._onToCurrency} value={this.state.toCurrency}>
                      {this.selectOptions.map(function(opt) {
                        return <option key={"to-" + opt.value} value={opt.value}>{opt.label}</option>;
                      })}
                    </select>
                  </div>,
                  <input type="number" placeholder={this.getIntlMessage('generic.amount')}
                         className="has-right-addon" ref="toAmount"
                         value={this.state.toAmount} onChange={this._onToAmount}
                         min="0.00000001" step="any" required/>)
                }
              </div>

            </div>

            <div className="row">
              <div className="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <LaddaButton className="button button-full action-button text-heavy"
                             title={this.getIntlMessage('convert.button_title')} type="submit"
                             buttonStyle="zoom-in" loading={this.state.submitting}>
                  {this.getIntlMessage('convert.button')}
                </LaddaButton>
                <div className="hint hint-center">{summary}</div>
              </div>
            </div>

          </form>

          <BoxMessage
            error={this.state.err ? true : false}
            msg={infoMsg} rawMsg={rawMsg}
            onHide={this.onHideMessage} />

          <Modal ref="modal" className="invoice-modal">
            <Invoice params={{id: this.state.info}} hideBottom={true} />
            <div className="invoice-modal-close invoice-outside">
              <a onClick={this.hideModal}>{this.getIntlMessage('generic.close')}</a>
            </div>
          </Modal>

        </div>

        {/* Right side (or bottom in smaller screens) */}
        <div className="col-xs-12 col-sm-4 col-lg-3 col-lg-offset-1">
          <div className="separator-m-2"></div>
          <Ticker pair={["BTC", "USD"]} />
          <div className="separator-m"></div>
          <Balance />
        </div>

      </div>
    </div>
  );

}


module.exports = template;
