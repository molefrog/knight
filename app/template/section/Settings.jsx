/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var helper = require('../helper/layout.jsx');


function template() {

  var accountType = null;
  var pubKeyRing = null;

  if (this.state.wallet && this.state.wallet.raw) {
    var raw = this.state.wallet.raw;

    accountType = this.formatMessage(
      this.getIntlMessage('settings.account_type_info'),
      {network: raw.network + (raw.n > 1 ? " multisig" : ""), m: raw.m, n: raw.n}
    );

    pubKeyRing = (raw.publicKeyRing || []).map(function(entry, index) {
      return <div key={"pub-" + index} className="settings-raw">{entry.xPubKey}</div>;
    });
  }

  return (
    <div className="settings">
      {helper.row(this.getIntlMessage('settings.account_type'), "col-xs-12 settings-raw", accountType)}
      {helper.row(this.getIntlMessage('settings.public_keyring'), "col-xs-12", pubKeyRing)}
    </div>
  );
}


module.exports = template;
