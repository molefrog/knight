/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var LaddaButton = require('react-ladda');
var url = require('url');

var BoxMessage = require('../../src/component/widget/BoxMessage.react');
var Balance = require('../../src/component/widget/Balance.react');
var PendingTx = require('../../src/component/widget/PendingTx.react');
var helper = require('../helper/layout.jsx');


function template() {

  if (!this.props.wallet) {
    console.error('no props received');
    return null;
  }

  /* Handle receiving a bitcoin:address[?amount=value] in the url. */
  var bitcoinUriRaw = this.getQuery().uri;
  var bitcoinUri;
  var bitcoinAddress;
  var bitcoinAmount;
  if (bitcoinUriRaw) {
    bitcoinUri = url.parse(bitcoinUriRaw, true);
    if (bitcoinUri.protocol === 'bitcoin:') {
      /* Using bitcoinUri.hostname would be much simpler than this, but
       * it is not case sensitive. */
      var matchAddress = bitcoinUriRaw.match(/bitcoin:([^\?]+)/i);
      if (matchAddress) {
        bitcoinAddress = matchAddress[1];
      }
      bitcoinAmount = bitcoinUri.query.amount;
    }
  }

  var boxMsg = this.state.err || this.state.info;
  if (boxMsg) {
    boxMsg = this.getIntlMessage(boxMsg);
  }

  return (
    <div className="user-page">

      <p>{this.getIntlMessage('send.title')}</p>
      <hr/>

      <div className="row">

        <div className="col-xs-12 col-sm-5 col-md-4">
          <form onSubmit={this._onSubmit} autoCorrect="off" autoCapitalize="none">
            {helper.row(
              this.getIntlMessage('generic.destination'), null,
              <input placeholder={this.getIntlMessage('generic.address')}
                     ref="address" type="text" name="address"
                     defaultValue={bitcoinAddress} required/>)}
            {helper.rowAddon(
              this.getIntlMessage('send.amount'), "BTC",
              <input placeholder={this.getIntlMessage('generic.amount')}
                     ref="amount" type="number" name="amount"
                     defaultValue={bitcoinAmount}
                     className="has-right-addon"
                     min="0.00000001" step="any" required/>)}
            {helper.row(
              null, "col-xs-7",
              <LaddaButton buttonStyle="zoom-in" loading={this.state.submitted}
                           type="submit" className="button button-full action-button text-heavy">
                {this.getIntlMessage('send.button')}
              </LaddaButton>)}
          </form>

          <BoxMessage
            error={this.state.err ? true : false}
            msg={boxMsg} rawMsg={boxMsg}
            onHide={this._hideMessage} />

        </div>

        <div className="col-xs-12 col-sm-6 col-sm-offset-1 col-md-7">
          <div className="row infoblock">
            <div className="col-xs-12 col-md-8 col-lg-6">
              <Balance />
            </div>
          </div>
          <div className="separator-p"></div>
          <PendingTx info={this.props.wallet.pending} client={this.props.wallet.client}/>
        </div>

      </div>

    </div>
  );
}


module.exports = template;
