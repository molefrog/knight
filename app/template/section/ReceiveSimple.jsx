/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var LaddaButton = require('react-ladda');
var classNames = require('classnames');
var helper = require('../helper/layout.jsx');


function template() {
  var addr;
  var addrInfo = null;

  if (this.state.addressInfo) {
    var showInfo = this.state.showAddressInfo;
    var createdOn = this.state.addressInfo.createdOn * 1000;

    addr = this.state.addressInfo.address;
    addrInfo = (
      <div className="rawblock infoblock-1">
        <a href="#" onClick={this._onToggleInfo}>
          {showInfo ?
            this.getIntlMessage('receive.simple.info_hide') :
            this.getIntlMessage('receive.simple.info_show')}
        </a>
        <div style={{display: showInfo ? "" : "none"}}>
          <p>{addr}</p>
          <p>{this.getIntlMessage('receive.simple.info_created_on')}: {
            this.formatDate(
              createdOn,
              {day: 'numeric', month: 'long', year: 'numeric',
               timeZoneName: 'short', hour: 'numeric', minute: 'numeric'}
            )}
          </p>
          <p>{this.getIntlMessage('receive.simple.info_derivation_path')}: {this.state.addressInfo.path}</p>
        </div>
      </div>
    );
  } else {
    addr = this.getIntlMessage('receive.simple.loading');
  }

  return (
      <div className="row">

        <div className="col-xs-12 col-sm-6 col-md-5">
          <form onSubmit={this._onSubmit} className="qr-form">
            {helper.rowAddon(
              this.getIntlMessage('receive.simple.label'),
              <LaddaButton buttonStyle="zoom-in" loading={this.state.submitted}
                           type="submit" className="right-addon button button-padding dark-button text-heavy"
                           title={this.getIntlMessage('receive.simple.button_title')}>
                +
              </LaddaButton>,
              <input type="text"
                     onClick={this._onGetAddress}
                     className={classNames("has-right-addon", "qr-address",
                                          {"qr-waiting": this.state.submitted})}
                     value={addr} readOnly />)}
            {helper.row(
              null, null,
              <div className={classNames("qr-container", "qr-address",
                                         {"qr-waiting": this.state.submitted})}
                   style={{minHeight: this.state.qrSize}}>
                <img src={this.state.qrSrc} />
              </div>)}
          </form>

          <div>
            {addrInfo}
          </div>
        </div>

      </div>
  );
}


module.exports = template;
