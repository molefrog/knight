serverconst = app/src/constant/ServerConstants.js


build:
	npm run-script build

build-intl:
	npm run-script build-intl

setup:
	test -e $(serverconst) || cp $(serverconst).orig $(serverconst)
	mkdir -p public/css public/html public/js public/font public/img
	cp app/index.html public/html
	cp app/font/* public/font/
	rm public/font/*.json
	cp app/img/* public/img/
	mv public/img/favicon.ico public/

all: setup build build-intl
	@echo "Remember to adjust ServerConstants.js and then run make build"
