/*eslint new-cap:0 */
"use strict";

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var redis = require('redis').createClient();

var port = 3344;


var deglet = io.of('/deglet').on('connection', function(socket) {

  console.log('user connected');

  socket.on('join', function(channel, param) {
    var result = {room: channel};
    var ok = false;

    if (channel === 'ticker') {
      ok = true;
    } else if (channel === 'transaction' && typeof param === 'string') {
      ok = true;
      channel = channel + '-' + param;
    } else if (channel === 'fiat-balance' && typeof param === 'string') {
      ok = true;
      channel = channel + '-' + param;
    }

    if (ok) {
      socket.join(channel);
      result.status = 'ok';
    } else {
      result.status = 'error';
      result.msg = 'unknown room';
    }
    socket.emit('join', result);
  });

  socket.on('disconnect', function() {
    console.log('user disconnected');
  });

});


redis.on('message', function(channel, msg) {
  console.log(channel, msg);
  var token;
  var obj = JSON.parse(msg);

  switch (channel) {
    case 'ticker': {
      deglet.to('ticker').emit('ticker', obj);
      break;
    }
    case 'fiat-balance': {
      /* XXX Convert uid to token. */
      token = obj.uid;
      deglet.to('fiat-balance-' + token).emit('fiat-balance', obj);
      break;
    }
    case 'transaction': {
      token = obj.uid;
      deglet.to('transaction-' + token).emit('transaction', obj);
      break;
    }
  }

});

redis.subscribe('ticker');
redis.subscribe('fiat-balance');
redis.subscribe('transaction');


http.listen(port, function() {
  console.log('listening on *:' + port);
});
