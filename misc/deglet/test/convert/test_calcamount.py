from decimal import Decimal

from deglet.api import convert_handler
from deglet.common.config import TRANSACTION


PAIR = ['BTC', 'USD']
MINBTC = TRANSACTION['limit']['BTC']['minimum']
MINUSD = TRANSACTION['limit']['USD']['minimum']


def test_known_BTC_to_USD():
    # Convert known BTC amount to USD.
    params = {
        'side_amount': MINBTC,  # satoshis
        'side_currency': 'BTC',
        'other_currency': 'USD',
        'rate': convert_handler._conversion_rate('BTC', PAIR)
    }

    # 1 BTC == params['rate'] USD
    res = params['side_amount'] / Decimal('1e8') * params['rate']

    usd_cents = convert_handler._calc_amount(**params)
    assert usd_cents == int(res * Decimal('1e2'))
    assert usd_cents >= 1


def test_known_USD_to_BTC():
    # Convert known USD amount to BTC.
    params = {
        'side_amount': MINUSD,
        'side_currency': 'USD',
        'other_currency': 'BTC',
        'rate': convert_handler._conversion_rate('USD', PAIR)
    }

    # 1 USD == 1 / params['rate'] BTC
    res = params['side_amount'] / Decimal('1e2') * params['rate']

    satoshis = convert_handler._calc_amount(**params)
    assert satoshis == int(res * Decimal('1e8'))
    assert satoshis > MINBTC
