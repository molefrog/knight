from collections import namedtuple

ERR = namedtuple("Error", ["error", "description"])


def err(error, description):
    instance = ERR(error, description)
    return instance._asdict()


class Error:

    InvalidID = err('BADID', 'ID is not valid')
    UnknownID = err('UNKID', 'ID is not known')

    UnknownPair = err('UNKPAIR', 'Pair specified is not supported')

    AddressInUse = err('ADDRINUSE', 'Address already used in an invoice or conversion')
    InvalidAddress = err('BADADDR', 'Address is not valid')
    SendFail = err('SEND', 'Failed to send the payment')
    StateUpdateFail = err('STATEUPDATE', 'Failed to update transaction status')

    UnknownSide = err('UNKSIDE', 'Side specified is not valid for conversions')
    UnknownCurrency = err('UNKCURR', 'Currency specified is not supported')
    NoConversion = err('NOCONV', 'No conversion')
    AlreadyStarted = err('STARTED', 'Countdown already started')

    InvalidBTCUSDPayment = err('BADPAY', 'Cannot pay for BTC -> USD transaction')
    InvalidUserState = err('BADSTATE', 'Inconsistent user state')

    TooLow = err('TOOLOW', 'Amount is too low')
    LowBalance = err('LOWBAL', 'Balance is not sufficient to perform this action')

    UnknownUser = err('UNKUSER', 'User not found')
    UserAlreadyPresent = err('EXIST', 'User already present')
    WalletComplete = err('COMPLETE', 'Wallet is already complete')
    NoBlob = err('NOBLOB', 'Blob is missing')
    CosignFail = err('COSIGN', 'Failed to sign')
    CosignJoin = err('JOIN', 'Failed to join wallet')
