import redis
from flask import Blueprint
from flask_restful import Api, Resource

from .error import Error
from ..common.ticker import calc_change


red = redis.StrictRedis()


class Ticker(Resource):

    def get(self, pair):
        data = red.hgetall('ticker:%s' % pair.upper())
        if not data:
            return Error.UnknownPair

        return calc_change(data)


api_blueprint = Blueprint('ticker', __name__)

api = Api(api_blueprint)
api.add_resource(Ticker, '/test/ticker/<string:pair>')
