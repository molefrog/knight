import json
import sqlite3

import requests
from flask import Blueprint, request, g
from flask_restful import Api, Resource, reqparse, abort

from .error import Error
from ..common.config import SQLDB_PATH, COSIGNER_SERVER

uid_parser = reqparse.RequestParser()
uid_parser.add_argument('uid', type=str, trim=True, required=True)
signup_parser = uid_parser.copy()
signup_parser.add_argument('username', type=str, trim=True, required=True)


def before_request():
    g.db = sqlite3.connect(SQLDB_PATH)


def teardown_request(exception):
    g.db.commit()
    g.db.close()


class UserSign(Resource):

    def post(self, uid, txid):
        dbres = g.db.execute("SELECT cowallet FROM walletblob WHERE uid=?",
                             (uid, )).fetchone()
        if not dbres:
            abort(403, message=Error.UnknownUser['error'])

        # Sign and broadcast the transaction specified by txid.
        data = {"wallet": dbres[0], "txid": txid}
        res = requests.post(COSIGNER_SERVER + '/sign',
                            data=json.dumps(data),
                            headers={'Content-Type': 'application/json'})
        content = res.json()
        if 'success' not in content:
            abort(401, message=Error.CosignFail['error'])

        return {'success': 'ok'}


class UserStore(Resource):

    def get(self, uid):
        res = g.db.execute("SELECT usdbalance FROM walletblob WHERE uid = ?",
                           (uid, )).fetchone()
        if res:
            return {'hi': uid, 'usdbalance': res[0]}
        else:
            abort(401, message=Error.UnknownUser['error'])

    def put(self, uid):
        blob = request.json.get('blob')
        secret = request.json.get('secret')
        if blob is None:
            abort(403, message=Error.NoBlob['error'])

        g.db.execute("UPDATE walletblob SET blob = ? WHERE uid = ?",
                     (blob, uid))

        if secret:
            # Join this wallet.
            res = requests.post(COSIGNER_SERVER + '/join',
                                data=json.dumps({"secret": secret}),
                                headers={'Content-Type': 'application/json'})
            content = res.json()
            if 'wallet' not in content:
                abort(401, message=Error.CosignJoin['error'])
            g.db.execute("UPDATE walletblob SET cowallet = ? WHERE uid = ?",
                         (content['wallet'], uid))

        return {uid: blob}


class UserLogin(Resource):

    def post(self):
        uid = uid_parser.parse_args(strict=True)['uid']

        dbres = g.db.execute("SELECT blob, usdbalance FROM walletblob WHERE uid=?",
                             (uid, )).fetchone()
        if not dbres:
            return Error.UnknownUser

        blob, usdbalance = dbres
        result = {'hi': uid, 'blob': blob, 'usdbalance': usdbalance}
        return result


class UserSignup(Resource):

    def post(self):
        info = signup_parser.parse_args(strict=True)
        uid, username = info['uid'], info['username']
        res = g.db.execute("SELECT COUNT(*) FROM walletblob WHERE username=?",
                           (username, )).fetchone()[0]
        if res:
            return Error.UserAlreadyPresent
        elif len(username) < 4:
            # Effectively reject usernames shorter than 4 characters,
            # maybe this could use a different message.
            return Error.UserAlreadyPresent

        blob = None
        usdbalance = 0
        g.db.execute("INSERT INTO walletblob "
                     "(uid, username, usdbalance, blob) "
                     "VALUES(?, ?, ?, ?)", (uid, username, usdbalance, blob))

        result = {'hi': uid, 'blob': blob, 'usdbalance': usdbalance}
        return result


api_blueprint = Blueprint('auth', __name__)
api_blueprint.before_request(before_request)
api_blueprint.teardown_request(teardown_request)

api = Api(api_blueprint)
api.add_resource(UserSignup, '/test/signup')
api.add_resource(UserLogin, '/test/login')
api.add_resource(UserStore, '/test/<string:uid>')
api.add_resource(UserSign, '/test/<string:uid>/<string:txid>')
