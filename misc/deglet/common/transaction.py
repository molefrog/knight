import time
import json
from decimal import Decimal

import redis
import rethinkdb as r
from flask import g

from .config import RDB_TABLE, TRANSACTION


DURATION = TRANSACTION['duration']


class TransactionStatus:
    WAITING_USER = 'waiting_user'
    PENDING = 'pending'
    PAID = 'paid'
    CONFIRMED = 'confirmed'
    EXPIRED = 'expired'
    CANCELED = 'canceled'


def update_status(trans, wait_user=True, conn=None):
    current_status = trans['status_info'][0]['status']

    if current_status == TransactionStatus.PENDING:
        return update_pending(trans, conn)
    elif current_status == TransactionStatus.WAITING_USER:
        return update_waitinguser(trans, wait_user, conn)


def update_pending(invoice, conn=None):
    red = redis.StrictRedis()

    new_status = None
    update = {}
    current = invoice['status_info'][0]
    assert current['status'] == TransactionStatus.PENDING, current['status']

    # Handle invoices and conversions.
    if 'received' in invoice['input']:
        key = 'received'
    elif 'sent' in invoice['input']:
        key = 'sent'
    else:
        key = None
    if 'received' in invoice['output']:
        out_key = 'received'
    else:
        out_key = None

    if current['expireAt']['epoch_time'] - time.time() <= 0:
        # Transaction expired.
        new_status = TransactionStatus.EXPIRED
        update['status'] = {
            'status': new_status,
            'date': current['expireAt']
        }
    elif key and invoice['input'][key]['total'] >= invoice['input']['amount']:
        # Transaction has been paid.
        new_status = TransactionStatus.PAID
        update['status'] = {
            'status': new_status,
            'date': r.now()
        }
    elif out_key and invoice['output'][out_key]['total'] >= invoice['output']['amount']:
        # Transaction has been paid.
        new_status = TransactionStatus.PAID
        update['status'] = {
            'status': new_status,
            'date': r.now()
        }
    else:
        # No changes.
        return None

    update_transaction(invoice['id'], update, conn)
    _publish_update(red, invoice, new_status)

    return new_status


def update_waitinguser(invoice, wait=False, conn=None):
    red = redis.StrictRedis()

    new_status = None
    created_at = invoice['status_info'][-1]
    assert created_at['status'] == 'waiting_user'
    current_status = invoice['status_info'][0]['status']
    if current_status != TransactionStatus.WAITING_USER:
        return None

    update = {}

    if created_at['expireAt']['epoch_time'] - time.time() <= 0:
        # Mark invoice as canceled.
        new_status = TransactionStatus.CANCELED
        update['status'] = {
            'status': new_status,
            'date': r.now()
        }

    elif not wait:
        # Start the countdown.
        pair = invoice['input']['currency'] + invoice['output']['currency']
        if pair != 'BTCBTC':
            # Calculate conversion rate and input amount.
            conversion_rate = red.hget('ticker:%s' % pair, 'last')
            output = Decimal(invoice['output']['amount']) / 100
            calculated_input = int(output / Decimal(conversion_rate) * Decimal('1e8'))
            print conversion_rate, calculated_input

            # XXX Missing reference id for the conversion rate.
            update['conversion'] = {
                'rate': conversion_rate
            }
            update['input'] = {
                'amount': calculated_input
            }

        new_status = TransactionStatus.PENDING
        update['status'] = {
            'status': new_status,
            'date': r.now(),
            'expireAt': r.now() + DURATION
        }

        # Create a key on redis that will expire in 7 minutes.
        # When that happens some process will be responsible for
        # requesting a status update for this transaction.
        red.set(invoice['id'], 1, ex=DURATION + 2)  # 2 extra seconds.

    if not update:
        return None

    update_transaction(invoice['id'], update, conn)
    _publish_update(red, invoice, new_status)

    return new_status


def update_transaction(iid, update, conn=None):
    update['last_update'] = r.now()
    if 'status' in update:
        status_update = update.pop('status')
        update['status_info'] = r.row['status_info'].prepend(status_update)

    conn = conn or g.rdb_conn
    result = r.table(RDB_TABLE).get(iid).update(update).run(conn)
    assert result['replaced'] == 1
    assert sum(result.values()) == 1


def _publish_update(red, trans, new_status):
    data = {'id': trans['id'], 'uid': trans['owner_id'], 'status': new_status}
    red.publish('transaction', json.dumps(data))
