import math
import random

_BASE = 33
_BASE_33 = '0123456789ABCDEFGHJKLMNPQRSTVWXYZ'  # Used in transaction IDs.
BASE_33 = {entry: i for i, entry in enumerate(_BASE_33)}

# ID is formed by
# RLEN chars + 1 checksum char (all in base 33)
RLEN = 9
ID_LEN = RLEN + 1


def lunh_checksum(digits):
    """
    Lunh mod N algorithm.
    """
    odd_pos = digits[-1::-2]
    even_pos = digits[-2::-2]

    checksum = sum(odd_pos)
    for digit in even_pos:
        value = digit * 2
        dsum = (value / _BASE) + (value % _BASE)
        checksum += dsum

    return checksum % _BASE


def valid_id(iid, check_base_only=False):
    """
    Return true if iid seems to be a valid id.
    """
    digits = [BASE_33.get(c) for c in iid.replace('-', '')]
    if None in digits:
        return False
    elif check_base_only:
        return True

    if len(digits) != ID_LEN:
        return False

    return lunh_checksum(digits) == 0


def gen_id():
    """
    Generate a case insensitive ID (n chars + 1 checksum char)
    divided in groups of 4 chars.
    """
    partial_id = ''.join(random.choice(_BASE_33) for _ in range(RLEN))
    checksum = lunh_checksum([BASE_33[c] for c in partial_id + '0'])
    checkcode = _BASE_33[(_BASE - checksum) % _BASE]
    iid = partial_id + checkcode

    # Separate the id by dashes to make easier to communicate it,
    # if necessary.
    pieces = int(math.ceil(ID_LEN / 4.))
    result = '-'.join(iid[i * 4:(i + 1) * 4] for i in range(pieces))
    return result


if __name__ == "__main__":
    for _ in range(10):
        iid = gen_id()
        print(iid)
        assert valid_id(iid)

        # Try mutating the id and verify the checksum.
        for i in xrange(len(iid)):
            if iid[i] == '-':
                continue
            x = _BASE_33[(BASE_33[iid[i]] + 1) % _BASE]
            test = iid[:i] + x + iid[i + 1:]
            assert not valid_id(test), test
