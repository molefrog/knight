from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import parse_command_line

from ..incoming.app import build_app


def main():
    app = build_app()

    parse_command_line()
    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen(6227)
    IOLoop.instance().start()


if __name__ == '__main__':
    main()
